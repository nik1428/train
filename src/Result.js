import React, { Component } from 'react';

class Result extends Component {
  render() {
    if (this.props.error) {
      return (
          <div className="red">{this.props.error}</div>
      )
    }

    if (this.props.section) {
      return (
          <div>Номер секции: {this.props.section}</div>
      )
    }
    return null;
  }
}

export default Result;