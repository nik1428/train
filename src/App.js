import React, { Component } from 'react';
import Result from './Result';

const placeInSection = 4; //по 4 основных места в вагоне
const placeInSide = 2; //по 2 в боковой
const sectionCnt = 9;
const firstPlace = 1;
const lastPlace = 54;

const baseCnt = placeInSection * sectionCnt; //кол-во основных мест

class App extends Component {
  constructor() {
    super();
    this.state = { place: null, section: null, error: null };
    this.choosePlace = this.choosePlace.bind(this);
  }

  // в вагоне 9 секций, не учитывая боковых 4x9 = 36 мест
  choosePlace(event) {
    this.setState({ error: null, section: null });
    let place = event.target.value;
    if (!place) {
      return;
    }

    if (isNaN(parseInt(place)) || (place >= firstPlace && place <= lastPlace) === false) {
       this.setState({ error: `Введите номер места от ${firstPlace} до ${lastPlace}` });
       return;
    }

    let section = Math.ceil(place / placeInSection); //округляем в большую сторону, для мест с 1 по 36 получаем номер секции

    //результат деления больше 9, следовательно боковое место
    if (section > sectionCnt) {
      var newPlaceNum = place - baseCnt; // номер бокового места в нумерации от 1 до 18
      section = sectionCnt + 1 - Math.ceil(newPlaceNum / placeInSide); //кратное 2м
    }

     this.setState({ section: section });
  }

  render() {
    return (
      <div className="App">
        <div>
          <label htmlFor="place">Введите номер места: </label>
          <input id="place" onChange={this.choosePlace} name="place" value={this.state.place}/>
        </div>
        <Result section={this.state.section} error={this.state.error}/>
      </div>
    );
  }
}

export default App;
